package com.ericcbm.androidtest.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.ericcbm.androidtest.R;
import com.ericcbm.androidtest.model.Forecast;
import com.ericcbm.androidtest.model.ForecastListItem;
import com.ericcbm.androidtest.model.dao.ForecastDAO;
import com.ericcbm.androidtest.model.dao.ForecatsItemDAO;
import com.ericcbm.androidtest.network.OpenWeatherService;
import com.ericcbm.androidtest.util.adapter.ForecastsListAdapter;
import com.orm.SugarRecord;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DetailActivity extends AppCompatActivity {

    public final static String EXTRA_CITY_ID = "com.ericcbm.androidtest.EXTRA_CITY_ID";
    public final static String EXTRA_CITY_NAME = "com.ericcbm.androidtest.EXTRA_CITY_NAME";

    private String cityName;
    private long cityId;

    @Bind(R.id.listview_forecasts)
    ListView listView;
    @Bind(R.id.progress_bar)
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);
        init();
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return false;
    }

    /*Internal Methods*/
    private void init() {
        initExtras();
        initToolbar();
        loadData();
    }

    private void initExtras() {
        Intent intent = getIntent();
        cityId = intent.getLongExtra(EXTRA_CITY_ID, 0);
        cityName = intent.getStringExtra(EXTRA_CITY_NAME);
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(cityName);
    }

    private void loadData() {
        Call<Forecast> call = openWeatherService().listForecasts(cityId, OpenWeatherService.KEY);
        call.enqueue(new Callback<Forecast>() {
            @Override
            public void onResponse(Call<Forecast> call, Response<Forecast> response) {
                int statusCode = response.code();
                if (statusCode == 200) {
                    Forecast forecast = response.body();
                    if (forecast.getCod().equals("200")) {
                        ForecastDAO forecastDAO = forecast.getForecastDAO();
                        SugarRecord.save(forecastDAO);
                        ForecatsItemDAO.deleteAll(ForecatsItemDAO.class, "forecast_dao = ?", Long.toString(cityId));
                        for (ForecastListItem forecastListItem : forecast.getList()) {
                            ForecatsItemDAO forecatsItemDAO = forecastListItem.getForecatsItemDAO(forecastDAO);
                            forecatsItemDAO.save();
                        }
                    }
                }
                showData(R.string.load_data_error);
                hideProgressbar();
            }

            @Override
            public void onFailure(Call<Forecast> call, Throwable t) {
                showData(R.string.load_data_network_error);
                hideProgressbar();
            }
        });
    }

    private void showData(int error) {
        ForecastDAO forecastDAO = SugarRecord.findById(ForecastDAO.class, cityId);
        if (forecastDAO != null) {
            ArrayList<ForecatsItemDAO> forecatsItemDAOArrayList = new ArrayList<>(forecastDAO.getListItems());
            ForecastsListAdapter adapter = new ForecastsListAdapter(this, forecatsItemDAOArrayList);
            listView.setAdapter(adapter);
        } else {
            showSnackBarMessage(error);
        }
    }

    private OpenWeatherService openWeatherService() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(OpenWeatherService.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(OpenWeatherService.class);
    }

    private void showSnackBarMessage(int resId ) {
        Snackbar.make(listView, getString(resId), Snackbar.LENGTH_LONG).show();
    }

    private void hideProgressbar() {
        progressBar.setVisibility(View.INVISIBLE);
    }

}
