package com.ericcbm.androidtest.model;

import com.ericcbm.androidtest.model.dao.ForecastDAO;

import java.util.List;

/**
 * Created by nd.ericmesquita on 08/03/2016.
 */
public class Forecast {
    private ForecastCity city;
    private String cod;
    private List<ForecastListItem> list;

    public ForecastCity getCity() {
        return city;
    }

    public void setCity(ForecastCity city) {
        this.city = city;
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public List<ForecastListItem> getList() {
        return list;
    }

    public void setList(List<ForecastListItem> list) {
        this.list = list;
    }

    public ForecastDAO getForecastDAO() {
        return new ForecastDAO(getCity().getId(), getCity().getName(), getCity().getCoord().getLon(), getCity().getCoord().getLat());
    }
}
