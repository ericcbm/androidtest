package com.ericcbm.androidtest.model.dao;

import com.orm.dsl.Table;

/**
 * Created by Eric Mesquita on 08/03/2016.
 */
@Table
public class CityDAO {
    private long id;
    private String name;
    private float temp;
    private String icon;

    public CityDAO() {
    }

    public CityDAO(long id, String name, float temp, String icon) {
        this.id = id;
        this.name = name;
        this.temp = temp;
        this.icon = icon;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getTemp() {
        return temp;
    }

    public void setTemp(float temp) {
        this.temp = temp;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Override
    public boolean equals(Object o) {
        return this.getId() == ((CityDAO) o).getId();
    }
}
