package com.ericcbm.androidtest.model.dao;

import com.orm.SugarRecord;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by nd.ericmesquita on 09/03/2016.
 */
public class ForecatsItemDAO extends SugarRecord {
    private long dt;
    private float day;
    private float min;
    private float max;
    private float night;
    private float eve;
    private float morn;
    private float pressure;
    private float humidity;
    private String main;
    private String description;
    private String icon;
    private float speed;
    private float deg;
    private float clouds;

    private ForecastDAO forecastDAO;

    public ForecatsItemDAO() {
    }

    public ForecatsItemDAO(long dt, float day, float min, float max, float night, float eve, float morn, float pressure, float humidity,
                           String main, String description, String icon, float speed, float deg, float clouds, ForecastDAO forecastDAO) {
        this.dt = dt;
        this.day = day;
        this.min = min;
        this.max = max;
        this.night = night;
        this.eve = eve;
        this.morn = morn;
        this.pressure = pressure;
        this.humidity = humidity;
        this.main = main;
        this.description = description;
        this.icon = icon;
        this.speed = speed;
        this.deg = deg;
        this.clouds = clouds;
        this.forecastDAO = forecastDAO;
    }

    public String getDt() {
        Date date = new java.util.Date(dt*1000);
        DateFormat df = DateFormat.getDateInstance();
        df.setTimeZone(TimeZone.getDefault());
        return df.format(date);
    }

    public void setDt(long dt) {
        this.dt = dt;
    }

    public float getDay() {
        return day;
    }

    public void setDay(float day) {
        this.day = day;
    }

    public float getMin() {
        return min;
    }

    public void setMin(float min) {
        this.min = min;
    }

    public float getMax() {
        return max;
    }

    public void setMax(float max) {
        this.max = max;
    }

    public float getNight() {
        return night;
    }

    public void setNight(float night) {
        this.night = night;
    }

    public float getEve() {
        return eve;
    }

    public void setEve(float eve) {
        this.eve = eve;
    }

    public float getMorn() {
        return morn;
    }

    public void setMorn(float morn) {
        this.morn = morn;
    }

    public float getPressure() {
        return pressure;
    }

    public void setPressure(float pressure) {
        this.pressure = pressure;
    }

    public float getHumidity() {
        return humidity;
    }

    public void setHumidity(float humidity) {
        this.humidity = humidity;
    }

    public String getMain() {
        return main;
    }

    public void setMain(String main) {
        this.main = main;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public float getDeg() {
        return deg;
    }

    public void setDeg(float deg) {
        this.deg = deg;
    }

    public float getClouds() {
        return clouds;
    }

    public void setClouds(float clouds) {
        this.clouds = clouds;
    }

    public ForecastDAO getForecastDAO() {
        return forecastDAO;
    }

    public void setForecastDAO(ForecastDAO forecastDAO) {
        this.forecastDAO = forecastDAO;
    }
}
