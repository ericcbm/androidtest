package com.ericcbm.androidtest.model;

import com.ericcbm.androidtest.model.dao.CityDAO;

import java.util.List;

/**
 * Created by nd.ericmesquita on 07/03/2016.
 */
public class City {
    private long id;
    private String name;
    private List<Weather> weather;
    private CityTemp main;
    private String cod;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Weather getWeather() {
        return weather.get(0);
    }

    public void setWeather(List<Weather> weather) {
        this.weather = weather;
    }

    public CityTemp getMain() {
        return main;
    }

    public void setMain(CityTemp main) {
        this.main = main;
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public CityDAO getCityDAO() {
        return new CityDAO(getId(), getName(), getMain().getTemp(), getWeather().getIcon());
    }
}
