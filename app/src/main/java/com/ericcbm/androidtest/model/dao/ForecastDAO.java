package com.ericcbm.androidtest.model.dao;

import com.orm.SugarRecord;
import com.orm.dsl.Table;

import java.util.List;

/**
 * Created by nd.ericmesquita on 09/03/2016.
 */
@Table
public class ForecastDAO {
    private long id;
    private String name;
    private float lon;
    private float lat;

    public ForecastDAO() {
    }

    public ForecastDAO(long id, String name, float lon, float lat) {
        this.id = id;
        this.name = name;
        this.lon = lon;
        this.lat = lat;
    }

    public List<ForecatsItemDAO> getListItems() {
        return ForecatsItemDAO.find(ForecatsItemDAO.class, "forecast_dao = ?", Long.toString(getId()));
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getLon() {
        return lon;
    }

    public void setLon(float lon) {
        this.lon = lon;
    }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }
}
