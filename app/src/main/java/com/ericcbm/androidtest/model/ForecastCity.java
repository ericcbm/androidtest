package com.ericcbm.androidtest.model;

/**
 * Created by nd.ericmesquita on 09/03/2016.
 */
public class ForecastCity {
    private long id;
    private String name;
    private ForecastCityCoord coord;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ForecastCityCoord getCoord() {
        return coord;
    }

    public void setCoord(ForecastCityCoord coord) {
        this.coord = coord;
    }
}
