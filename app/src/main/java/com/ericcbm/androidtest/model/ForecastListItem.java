package com.ericcbm.androidtest.model;

import com.ericcbm.androidtest.model.dao.ForecastDAO;
import com.ericcbm.androidtest.model.dao.ForecatsItemDAO;

import java.util.List;

/**
 * Created by nd.ericmesquita on 09/03/2016.
 */
public class ForecastListItem {
    private long dt;
    private ForecastTemp temp;
    private float pressure;
    private float humidity;
    private List<Weather> weather;
    private float speed;
    private float deg;
    private float clouds;

    public long getDt() {
        return dt;
    }

    public void setDt(long dt) {
        this.dt = dt;
    }

    public ForecastTemp getTemp() {
        return temp;
    }

    public void setTemp(ForecastTemp temp) {
        this.temp = temp;
    }

    public float getPressure() {
        return pressure;
    }

    public void setPressure(float pressure) {
        this.pressure = pressure;
    }

    public float getHumidity() {
        return humidity;
    }

    public void setHumidity(float humidity) {
        this.humidity = humidity;
    }

    public Weather getWeather() {
        return weather.get(0);
    }

    public void setWeather(List<Weather> weather) {
        this.weather = weather;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public float getDeg() {
        return deg;
    }

    public void setDeg(float deg) {
        this.deg = deg;
    }

    public float getClouds() {
        return clouds;
    }

    public void setClouds(float clouds) {
        this.clouds = clouds;
    }


    public ForecatsItemDAO getForecatsItemDAO(ForecastDAO forecastDAO) {
        return new ForecatsItemDAO(getDt(), getTemp().getDay(), getTemp().getMin(), getTemp().getMax(), getTemp().getNight(), getTemp().getEve(), getTemp().getMorn(),
                getPressure(), getHumidity(), getWeather().getMain(), getWeather().getDescription(), getWeather().getIcon(), getSpeed(), getDeg(), getClouds(), forecastDAO);
    }
}
