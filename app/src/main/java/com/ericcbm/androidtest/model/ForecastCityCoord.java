package com.ericcbm.androidtest.model;

/**
 * Created by nd.ericmesquita on 09/03/2016.
 */
public class ForecastCityCoord {
    private float lon;
    private float lat;

    public float getLon() {
        return lon;
    }

    public void setLon(float lon) {
        this.lon = lon;
    }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }
}