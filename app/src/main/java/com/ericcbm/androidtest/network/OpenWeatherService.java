package com.ericcbm.androidtest.network;

import com.ericcbm.androidtest.model.City;
import com.ericcbm.androidtest.model.Forecast;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Eric Mesquita on 08/03/2016.
 */
public interface OpenWeatherService {
    String BASE_URL = "http://api.openweathermap.org";
    String KEY = "3fed727e458bcfd29fd2a815777eaca2";

    @GET("data/2.5/weather?units=metric")
    Call<City> cityWeather(@Query("q") String cityName, @Query("appid") String key);

    @GET("data/2.5/weather?units=metric")
    Call<City> cityWeather(@Query("id") long cityId, @Query("appid") String key);

    @GET("data/2.5/forecast/daily?units=metric&cnt=5")
    Call<Forecast> listForecasts(@Query("id") long cityId, @Query("appid") String key);
}
