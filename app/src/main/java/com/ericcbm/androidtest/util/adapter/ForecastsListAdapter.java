package com.ericcbm.androidtest.util.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ericcbm.androidtest.R;
import com.ericcbm.androidtest.model.dao.ForecatsItemDAO;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Eric Mesquita on 10/03/2016.
 */
public class ForecastsListAdapter extends ArrayAdapter<ForecatsItemDAO> {

    private Context context;

    public ForecastsListAdapter(Context context, ArrayList<ForecatsItemDAO> forecastItemDAOAList) {
        super(context, 0, forecastItemDAOAList);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ForecatsItemDAO forecatsItemDAO = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.view_forecast, parent, false);
        }

        TextView textDate = (TextView) convertView.findViewById(R.id.text_date);
        TextView textTempMax = (TextView) convertView.findViewById(R.id.text_temp_max);
        TextView textTempMin = (TextView) convertView.findViewById(R.id.text_temp_min);
        ImageView imageForecast = (ImageView) convertView.findViewById(R.id.image_forecast);
        TextView textForecast = (TextView) convertView.findViewById(R.id.text_forecast);

        String date;
        switch (position) {
            case 0:
                date = context.getString(R.string.today_date, forecatsItemDAO.getDt());
                break;
            case 1:
                date = context.getString(R.string.tomorrow_date, forecatsItemDAO.getDt());
                break;
            default:
                date = forecatsItemDAO.getDt();
        }
        textDate.setText(date);
        textTempMax.setText(context.getString(R.string.temp2, forecatsItemDAO.getMax()));
        textTempMin.setText(context.getString(R.string.temp2, forecatsItemDAO.getMin()));
        Picasso.with(context).load(forecatsItemDAO.getIcon())
                .into(imageForecast);
        textForecast.setText(forecatsItemDAO.getMain());
        return convertView;
    }
}
