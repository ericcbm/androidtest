package com.ericcbm.androidtest.util.adapter;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ericcbm.androidtest.R;
import com.ericcbm.androidtest.model.dao.CityDAO;
import com.ericcbm.androidtest.util.listner.SwipeableRecyclerViewTouchListener;
import com.orm.SugarRecord;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by nd.ericmesquita on 07/03/2016.
 */
public class CitiesListAdapter extends RecyclerView.Adapter<CitiesListAdapter.ViewHolder> implements SwipeableRecyclerViewTouchListener.SwipeListener {
    private List<CityDAO> citiesDAOList;
    private Context context;
    private final OnItemClickListener itemClickListener;

    @Override
    public boolean canSwipeLeft(int position) {
        return true;
    }

    @Override
    public boolean canSwipeRight(int position) {
        return true;
    }

    @Override
    public void onDismissedBySwipeLeft(RecyclerView recyclerView, int[] reverseSortedPositions) {
        onDismiss(recyclerView, reverseSortedPositions);
    }

    @Override
    public void onDismissedBySwipeRight(RecyclerView recyclerView, int[] reverseSortedPositions) {
        onDismiss(recyclerView, reverseSortedPositions);
    }

    private void onDismiss(final RecyclerView recyclerView, int[] reverseSortedPositions) {
        for (final int position : reverseSortedPositions) {
            final CityDAO cityDAO = citiesDAOList.get(position);
            SugarRecord.delete(cityDAO);
            citiesDAOList.remove(position);
            notifyItemRemoved(position);
            Snackbar
                    .make(recyclerView, context.getString(R.string.city_deleted_message), Snackbar.LENGTH_LONG)
                    .setAction(context.getString(R.string.undo), new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            SugarRecord.save(cityDAO);
                            citiesDAOList.add(position, cityDAO);
                            notifyItemInserted(position);
                            recyclerView.scrollToPosition(position);
                            Snackbar.make(recyclerView, context.getString(R.string.city_restored_message), Snackbar.LENGTH_SHORT).show();
                        }
                    }).show();
        }
        notifyDataSetChanged();
    }

    public interface OnItemClickListener {
        void onItemClick(CityDAO cityDAO, int position);
    }

    public CitiesListAdapter(Context context, List<CityDAO> citiesList, final OnItemClickListener itemClickListener) {
        this.citiesDAOList = citiesList;
        this.context = context;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public CitiesListAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_city, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int i) {
        final CityDAO cityDAO = citiesDAOList.get(i);

        Picasso.with(context).load(cityDAO.getIcon())
                .into(viewHolder.imageView);
        viewHolder.textCityView.setText(cityDAO.getName());
        viewHolder.textTempView.setText(context.getString(R.string.temp, cityDAO.getTemp()));
        viewHolder.itemView.setClickable(true);
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClickListener.onItemClick(cityDAO, i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (null != citiesDAOList ? citiesDAOList.size() : 0);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.image_forecast) ImageView imageView;
        @Bind(R.id.text_city_name) TextView textCityView;
        @Bind(R.id.text_forecast_temp) TextView textTempView;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
