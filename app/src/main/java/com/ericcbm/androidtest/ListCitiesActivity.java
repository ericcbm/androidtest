package com.ericcbm.androidtest;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.afollestad.materialdialogs.MaterialDialog;
import com.ericcbm.androidtest.activity.DetailActivity;
import com.ericcbm.androidtest.model.City;
import com.ericcbm.androidtest.model.dao.CityDAO;
import com.ericcbm.androidtest.network.OpenWeatherService;
import com.ericcbm.androidtest.util.adapter.CitiesListAdapter;
import com.ericcbm.androidtest.util.listner.SwipeableRecyclerViewTouchListener;
import com.orm.SugarRecord;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ListCitiesActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, CitiesListAdapter.OnItemClickListener {

    private final static int FILTER_BY_NAME = 0;
    private final static int FILTER_BY_MAX_TEMP = 1;
    private final static int FILTER_BY_MIN_TEMP = 2;
    private int filterSelectedChoice = FILTER_BY_NAME;

    @Bind(R.id.swipe_refresh_list)
    SwipeRefreshLayout swipeRefreshList;
    @Bind(R.id.recycler_list)
    RecyclerView recyclerList;
    @Bind(R.id.progress_bar)
    ProgressBar progressBar;
    @Bind(R.id.fab)
    FloatingActionButton fab;

    private CitiesListAdapter citiesListAdapter;
    private MaterialDialog progressDialog;
    private SwipeableRecyclerViewTouchListener swipeTouchListener;

    //load cities state
    private int finishedCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //SugarRecord.deleteAll(CityDAO.class);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_cities);
        ButterKnife.bind(this);
        init();
    }

    @Override
    public void onRefresh() {
        loadCities();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_list_cities, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_filter) {
            showFilterDialod();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(CityDAO cityDAO, int position) {
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra(DetailActivity.EXTRA_CITY_ID, cityDAO.getId());
        intent.putExtra(DetailActivity.EXTRA_CITY_NAME, cityDAO.getName());
        startActivity(intent);
    }

    /*Internal Methods*/
    private void init() {
        initToolbar();
        initSwipeRefresh();
        initFloatingActionButton();
        initList();
    }

    private void initList() {
        final LinearLayoutManager recyclerLayoutManager = new LinearLayoutManager(this);
        recyclerList.setLayoutManager(recyclerLayoutManager);
        loadCities();
    }

    private void initFloatingActionButton() {
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showNewCityDialog();
            }
        });
    }

    private void initSwipeRefresh() {
        swipeRefreshList.setOnRefreshListener(this);
        swipeRefreshList.setColorSchemeColors(getResources().getIntArray(R.array.refreshProgressbarColors));
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.cities_list_toolbar_title));
    }

    private void loadCities() {
        List<CityDAO> cityDAOList = loadCitiesFromDB();
        if (cityDAOList.isEmpty()) {
            showCities(cityDAOList, 0);
            hideProgressbar();
            showRefreshProgressbar(false);
            dismissProgressDialog();
            showSnackBarMessage(R.string.no_city_message);
            return;
        }

        final int listSize = cityDAOList.size();
        finishedCount = 0;

        OpenWeatherService service = openWeatherService();
        for (CityDAO cityDAO : cityDAOList) {
            Call<City> call = service.cityWeather(cityDAO.getId(), OpenWeatherService.KEY);
            call.enqueue(new Callback<City>() {
                @Override
                public void onResponse(Call<City> call, Response<City> response) {
                    int statusCode = response.code();
                    //Log.d("ListCities", "statusCode: " + Integer.toString(statusCode));
                    if (statusCode == 200) {
                        City city = response.body();
                        if (city.getCod().equals("200")) {
                            CityDAO cityDAO = city.getCityDAO();
                            SugarRecord.save(cityDAO);
                        }
                    }
                    finishedCount++;
                    if (finishedCount == listSize) {
                        showCities(loadCitiesFromDB(), 0);
                        hideProgressbar();
                        showRefreshProgressbar(false);
                        dismissProgressDialog();
                    }
                }

                @Override
                public void onFailure(Call<City> call, Throwable t) {
                    finishedCount++;
                    if (finishedCount == listSize) {
                        showCities(loadCitiesFromDB(), 0);
                        hideProgressbar();
                        showRefreshProgressbar(false);
                        dismissProgressDialog();
                    }
                }
            });
        }
    }
    private List<CityDAO> loadCitiesFromDB() {
        String orderby = "name ASC";
        switch (filterSelectedChoice) {
            case FILTER_BY_NAME:
                orderby = "name ASC";
                break;
            case FILTER_BY_MAX_TEMP:
                orderby = "temp DESC";
                break;
            case FILTER_BY_MIN_TEMP:
                orderby = "temp ASC";
                break;
        }
        return SugarRecord.find(CityDAO.class, null, null, null, orderby, null);
    }

    private void showCities(List<CityDAO> citiesDAOList, int position) {
        citiesListAdapter = new CitiesListAdapter(this, citiesDAOList, this);
        recyclerList.setHasFixedSize(true);
        recyclerList.setAdapter(citiesListAdapter);
        recyclerList.scrollToPosition(position);

        if (swipeTouchListener != null)
            recyclerList.removeOnItemTouchListener(swipeTouchListener);

        swipeTouchListener =
                new SwipeableRecyclerViewTouchListener(recyclerList, citiesListAdapter);

        recyclerList.addOnItemTouchListener(swipeTouchListener);
    }

    private void saveNewCity(CharSequence input) {
        showProgressDialog();
        Call<City> call = openWeatherService().cityWeather(input.toString(), OpenWeatherService.KEY);
        call.enqueue(new Callback<City>() {
            @Override
            public void onResponse(Call<City> call, Response<City> response) {
                int statusCode = response.code();
                //Log.d("NewCity", "statusCode: " + Integer.toString(statusCode));
                if (statusCode == 200) {
                    City city = response.body();
                    if (city.getCod().equals("200")) {
                        CityDAO cityDAO = city.getCityDAO();
                        SugarRecord.save(cityDAO);
                        List<CityDAO> cityDAOList = loadCitiesFromDB();
                        showCities(loadCitiesFromDB(), cityDAOList.indexOf(cityDAO));
                        showSnackBarMessage(R.string.city_added_message, cityDAO.getName());
                    } else {
                        showSnackBarMessage(R.string.new_city_error);
                    }
                } else {
                    showSnackBarMessage(R.string.new_city_error);
                }
                dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<City> call, Throwable t) {
                showSnackBarMessage(R.string.new_city_network_error);
                dismissProgressDialog();
            }
        });
    }

    private OpenWeatherService openWeatherService() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(OpenWeatherService.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(OpenWeatherService.class);
    }

    private void showNewCityDialog() {
        new MaterialDialog.Builder(this)
                .title(R.string.dialog_new_city_title)
                .positiveText(R.string.dialog_positive_text)
                .negativeText(R.string.dialog_negative_text)
                .inputType(InputType.TYPE_CLASS_TEXT |
                        InputType.TYPE_TEXT_FLAG_CAP_WORDS)
                .input(null, null, false, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(MaterialDialog dialog, CharSequence input) {
                        saveNewCity(input);
                    }
                })
                .show();
    }

    private void showProgressDialog() {
        progressDialog = new MaterialDialog.Builder(ListCitiesActivity.this)
                .title(R.string.dialog_progressbar_new_city_title)
                .content(R.string.dialog_progress_subtitle)
                .progress(true, 0)
                .show();
    }

    private void showFilterDialod() {
        new MaterialDialog.Builder(this)
                .title(R.string.dialog_filter_title)
                .items(R.array.array_dialog_filter_choices)
                .itemsCallbackSingleChoice(filterSelectedChoice, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        filterSelectedChoice = which;
                        showCities(loadCitiesFromDB(), 0);
                        return true;
                    }
                })
                .positiveText(R.string.dialog_positive_text)
                .negativeText(R.string.dialog_negative_text)
                .show();
    }

    private void dismissProgressDialog() {
        if (progressDialog != null) progressDialog.dismiss();
    }

    private void hideProgressbar() {
        progressBar.setVisibility(View.INVISIBLE);
    }

    private void showRefreshProgressbar(final boolean show) {
        swipeRefreshList.setRefreshing(show);
    }

    private void showSnackBarMessage(int resId ) {
        Snackbar.make(fab, getString(resId), Snackbar.LENGTH_LONG).show();
    }

    private void showSnackBarMessage(int resId, String text) {
        Snackbar.make(fab, getString(resId, text), Snackbar.LENGTH_LONG).show();
    }
}
